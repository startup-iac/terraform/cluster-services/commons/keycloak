
module "keycloak" {
  source = "./keycloak-bitnami"

  namespace                   = var.namespace
  chart_version               = var.chart_version
  dns_name                    = var.dns_name
  cluster_issuer_name         = var.cluster_issuer_name
  subject_organizations       = var.subject_organizations
  subject_organizationalunits = var.subject_organizationalunits
  init_realms                 = var.init_realms
}
# For more info view:
# https://artifacthub.io/packages/helm/bitnami/keycloak

locals {
  secret_name = "${var.dns_name}-tls"
  values_file = "values-${var.chart_version}.yaml"
}

data "template_file" "values" {
  template = "${file("${path.module}/${local.values_file}")}"
  vars = {
    INIT_REALMS = indent(6, var.init_realms)
  }
}

resource "helm_release" "this" {
  name             = "keycloak"
  namespace        = var.namespace
  version          = var.chart_version

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "keycloak"

  values = [
    data.template_file.values.rendered
  ]

  set {
    name  = "tls.existingSecret"
    value = local.secret_name
  }

  set {
    name  = "ingress.hostname"
    value = var.dns_name
  }

  set {
    name  = "adminIngress.hostname"
    value = "console-${var.dns_name}"
  }

  # Ingress Annotations
  set {
    name  = "ingress.annotations.cert-manager\\.io/cluster-issuer"
    value = var.cluster_issuer_name
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/common-name"
    value = var.dns_name
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/subject-organizations"
    value = var.subject_organizations
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/subject-organizationalunits"
    value = var.subject_organizationalunits
  }

  # Admin Ingress Annotations
  set {
    name  = "adminIngress.annotations.cert-manager\\.io/cluster-issuer"
    value = var.cluster_issuer_name
  }  

  set {
    name  = "adminIngress.annotations.cert-manager\\.io/common-name"
    value = "console-${var.dns_name}"
  }  

  set {
    name  = "adminIngress.annotations.cert-manager\\.io/subject-organizations"
    value = var.subject_organizations
  }  

  set {
    name  = "adminIngress.annotations.cert-manager\\.io/subject-organizationalunits"
    value = var.subject_organizationalunits
  }
 

}


